/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.interfaceproject;

/**
 *
 * @author Maintory_
 */
public class Test {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine NO.1");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("DOGE",4);
        Runable[] runables = {dog,plane};
        for(Runable r:runables){
            r.run();
        }
       Flyable[] flyables = {plane,bat};
        for(Flyable f:flyables){
            f.fly();
        }
    }
}
